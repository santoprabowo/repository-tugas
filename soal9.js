let userData = {

    nama: `Farhan`,

    user_type: `free`,

};
const functionPromise = userData => {
    return new Promise((resolve, reject) =>{
        if(userData == `pro`){
            resolve(`pro`);
        } else{
            reject(`free`);
        }
    });
};

functionPromise(`pro`)
    .then(value => {
        console.log(`Silahkan Masuk dan Buat Produk yang Kamu Inginkan`);
        console.log(value);
    })
    .catch(error =>{
        console.log(`Maaf kamu tidak punya hak untuk mengakses ini`);
        console.log(error);
    });