const promiseFunction = category => {
    return new Promise((resolve, reject) => {
        let bootcampClass = [
            { name: 'React JS', category: 'development' },
            { name: 'Laravel', category: 'development' },
            { name: 'Vue JS', category: 'development' },
            { name: 'Node JS', category: 'development' },
            { name: 'UI/UX Design', category: 'design' },
            { name: 'Illustrator', category: 'design' },
            { name: 'Data Science', category: 'data' },
            { name: 'Data Analyst', category: 'data' },
            { name: 'Machine Learning', category: 'data' },
            { name: 'Digital Marketing', category: 'business' },
            { name: 'Business Development', category: 'business' },
        ];
        let filterClass = bootcampClass.filter(value => value.category === category);
        if (filterClass.length > 0) {
            resolve(filterClass);
        } else {
            resolve('Category tidak ditemukan');
        }
    });
}
promiseFunction(category);
// promiseFunction(`architect`)
// promiseFunction(`design`)